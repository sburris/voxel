mod blocks;
mod interact;
mod player;
mod ui;
mod world;

use crate::interact::InteractPlugin;
use crate::player::PlayerPlugin;
use crate::ui::UiPlugin;
use crate::world::setup_world;
use bevy::prelude::*;

#[bevy_main]
fn main() {
    App::build()
        .insert_resource(Msaa { samples: 8 })
        .insert_resource(ClearColor(Color::BLACK))
        .add_plugins(DefaultPlugins)
        .add_plugin(PlayerPlugin)
        .add_plugin(InteractPlugin)
        .add_plugin(UiPlugin)
        .add_startup_system(setup_world.system())
        .run();
}
