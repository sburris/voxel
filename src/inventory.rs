use bevy::prelude::*;
use crate::blocks::BlockBundle;

#[derive(Default)]
pub struct Hotbar(pub [Option<Entity>; 9]);
pub struct SelectedSlot(pub u8);

fn setup_hotbar(
    mut hotbar: ResMut<Hotbar>,
    query: Query<Entity, With<BlockBundle>>,
)
{
    for (i, block_entity) in query.iter().enumerate() {
        hotbar.0[i as usize] = Some(block_entity);
        println!("adding {:?} to hotbar", block_entity);
    }
}

pub struct InventoryPlugin;
impl Plugin for InventoryPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.init_resource::<Hotbar>()
        .insert_resource(SelectedSlot(0))
        .add_startup_system(setup_hotbar.system());
    }
}
