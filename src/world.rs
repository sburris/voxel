use bevy::prelude::*;

pub fn setup_world(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    commands
        .spawn_bundle(PbrBundle {
            mesh: meshes.add(Mesh::from(shape::Plane { size: 128.0 })),
            material: materials.add(Color::rgb(0.4, 0.4, 0.4).into()),
            transform: Transform::from_translation(-Vec3::Y),
            ..Default::default()
        })
        .insert_bundle(LightBundle {
            transform: Transform::from_translation(Vec3::new(45., 16., 0.)),
            light: Light {
                color: Color::rgb(1., 0., 0.),
                ..Default::default()
            },
            ..Default::default()
        })
        .insert_bundle(LightBundle {
            transform: Transform::from_translation(Vec3::new(0., 16., -32.)),
            light: Light {
                color: Color::rgb(0., 1., 0.),
                ..Default::default()
            },
            ..Default::default()
        })
        .insert_bundle(LightBundle {
            transform: Transform::from_translation(Vec3::new(0., 16., 32.)),
            light: Light {
                color: Color::rgb(0., 0., 1.),
                ..Default::default()
            },
            ..Default::default()
        });
}
