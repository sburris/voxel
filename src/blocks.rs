use bevy::prelude::*;

#[derive(Default)]
pub struct Block;

#[derive(Default)]
pub struct BlockName(pub String);

#[derive(Bundle, Default)]
pub struct BlockBundle {
    pub name: BlockName,
    pub texture: Handle<Texture>,
    pub _b: Block,
}
