use bevy::prelude::*;

fn setup_ui(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    commands
        .spawn_bundle(UiCameraBundle::default())
        .insert_bundle(ImageBundle {
            // Crosshair
            style: Style {
                size: Size::new(Val::Px(12.0), Val::Px(12.0)),
                position_type: PositionType::Absolute,
                position: Rect::all(Val::Percent(49.5)),
                ..Default::default()
            },
            material: materials.add(asset_server.load("textures/crosshair.png").into()),
            ..Default::default()
        })
        .insert_bundle(NodeBundle {
            // Hotbar
            style: Style {
                size: Size::new(Val::Percent(100.0), Val::Percent(5.0)),
                ..Default::default()
            },
            material: materials.add(Color::NONE.into()),
            ..Default::default()
        })
        .with_children(|parent| {
            for slot in 1..9 {
                parent.spawn_bundle(ImageBundle {
                    style: Style {
                        size: Size::new(Val::Px(60.0), Val::Px(60.0)),
                        position: Rect {
                            left: Val::Px(slot as f32 * 60.0),
                            right: Val::Px(slot as f32 * 60.0),
                            ..Default::default()
                        },
                        ..Default::default()
                    },
                    material: materials.add(asset_server.load("textures/hotbar.png").into()),
                    ..Default::default()
                });
            }
        })
        .insert_bundle(ImageBundle {
            style: Style {
                size: Size::new(Val::Px(60.0), Val::Px(60.0)),
                position_type: PositionType::Absolute,
                position: Rect {
                    left: Val::Px(0.0),
                    right: Val::Px(0.0),
                    ..Default::default()
                },
                ..Default::default()
            },
            material: materials.add(asset_server.load("textures/hotbar_active.png").into()),
            ..Default::default()
        })
        .insert(ActiveSlot);
}

#[derive(Default, Debug)]
pub struct HotbarSlot(pub u8);
struct ActiveSlot;

fn update_hotbar(
    keys: Res<Input<KeyCode>>,
    mut slot: ResMut<HotbarSlot>,
    mut query: Query<(&ActiveSlot, &mut Style)>,
) {
    for key in keys.get_pressed() {
        match key {
            KeyCode::Key1 => slot.0 = 1,
            KeyCode::Key2 => slot.0 = 2,
            KeyCode::Key3 => slot.0 = 3,
            KeyCode::Key4 => slot.0 = 4,
            KeyCode::Key5 => slot.0 = 5,
            KeyCode::Key6 => slot.0 = 6,
            KeyCode::Key7 => slot.0 = 7,
            KeyCode::Key8 => slot.0 = 8,
            KeyCode::Key9 => slot.0 = 9,
            _ => (),
        }

        for (_active_slot, mut style) in query.iter_mut() {
            style.position = Rect {
                left: Val::Px(slot.0 as f32 * 60.0 - 60.0),
                right: Val::Px(slot.0 as f32 * 60.0 - 60.0),
                ..Default::default()
            }
        }
    }
}

pub struct UiPlugin;
impl Plugin for UiPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_startup_system(setup_ui.system())
            .add_system(update_hotbar.system())
            .insert_resource(HotbarSlot(1));
    }
}
