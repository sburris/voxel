use crate::blocks::{Block, BlockBundle, BlockName};
use crate::player::RaycastSet;
use bevy::prelude::*;
use bevy_mod_raycast::*;

#[derive(Default)]
struct Aim(Transform);

#[derive(Default)]
struct PlaceLocation(Transform);

struct BlankAim(bool);

fn update_aim(
    mut aim: ResMut<Aim>,
    mut blankaim: ResMut<BlankAim>,
    mut place_loc: ResMut<PlaceLocation>,
    mut query: Query<&RayCastSource<RaycastSet>>,
    mut block: Query<(&Transform, &Block)>,
    windows: Res<Windows>,
) {
    let window = windows.get_primary().unwrap();
    for source in query.iter_mut() {
        if window.cursor_locked() {
            if let Some((entity, intersection)) = source.intersect_top() {
                *blankaim = BlankAim(intersection.distance() >= 4.5);

                if let Ok((transform, _block)) = block.get_mut(entity) {
                    aim.0.translation = transform.translation.round();
                }

                place_loc.0.translation = intersection.position().round();
            } else {
                *blankaim = BlankAim(true);
            }
        }
    }
}

fn place_block(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    btns: Res<Input<MouseButton>>,
    place_loc: Res<PlaceLocation>,
) {
    // TODO: startup system?
    let texture_handle = asset_server.load("textures/block.png");

    let first_block = BlockBundle {
        name: BlockName(String::from("First Block")),
        texture: texture_handle,
        ..Default::default()
    };

    let material_handle = materials.add(StandardMaterial {
        base_color_texture: Some(first_block.texture),
        ..Default::default()
    });

    // TODO: check blankaim once placing initial block not impossible
    if btns.just_pressed(MouseButton::Right) {
        commands
            .spawn_bundle(PbrBundle {
                mesh: meshes.add(Mesh::from(shape::Cube { size: 1.0 })),
                transform: place_loc.0,
                material: material_handle,
                ..Default::default()
            })
            .insert(RayCastMesh::<RaycastSet>::default())
            .insert(Block);
    }
}

fn break_block(
    mut commands: Commands,
    mut query: Query<(Entity, &Block, &mut Transform)>,
    btns: Res<Input<MouseButton>>,
    aim: Res<Aim>,
    blankaim: Res<BlankAim>,
) {
    for (entity_block, _block, transform) in query.iter_mut() {
        if btns.just_pressed(MouseButton::Left)
            && transform.translation.round() == aim.0.translation
            && !blankaim.0
        {
            commands.entity(entity_block).despawn();
        }
    }
}

struct Hl;

fn setup_highlight(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    asset_server: Res<AssetServer>,
) {
    let texture_handle = asset_server.load("textures/hl.png");

    let material_handle = materials.add(StandardMaterial {
        base_color_texture: Some(texture_handle),
        ..Default::default()
    });

    commands
        .spawn_bundle(PbrBundle {
            mesh: meshes.add(Mesh::from(shape::Cube { size: 1.1 })),
            material: material_handle,
            visible: Visible {
                is_visible: false,
                is_transparent: true,
            },
            ..Default::default()
        })
        .insert(Hl);
}

fn highlight_block(
    mut query: Query<(Entity, &Block, &mut Transform)>,
    // mut hl: Query<(Entity, &Hl, &mut Transform, &mut Visible)>,
    aim: Res<Aim>,
    blankaim: Res<BlankAim>,
) {
    // for (_entity, _hl, mut hl_transform, mut visible) in hl.iter_mut() {
    //     if query.iter_mut().len() != 0 {
    //         for (_entity_block, _block, transform) in query.iter_mut() {
    //             if transform.translation.round() == aim.0.translation {
    //                 *hl_transform = aim.0;
    //             }
    //             *visible = Visible {
    //                 is_visible: !blankaim.0,
    //                 is_transparent: true,
    //             }
    //         }
    //     } else {
    //         *visible = Visible {
    //             is_visible: !blankaim.0,
    //             is_transparent: true,
    //         }
    //     }
    // }
}

pub struct InteractPlugin;
impl Plugin for InteractPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.insert_resource(Aim::default())
            .insert_resource(PlaceLocation::default())
            .insert_resource(BlankAim(true))
            .add_startup_system(setup_highlight.system())
            .add_system(update_aim.system())
            .add_system(place_block.system())
            .add_system(highlight_block.system())
            .add_system(break_block.system());
    }
}
